package com.kolyadruz.dabaandriver.mvp;

import com.kolyadruz.dabaandriver.app.Api;
import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.mvp.models.ActiveOrderData;
import com.kolyadruz.dabaandriver.mvp.models.AutoLoginData;
import com.kolyadruz.dabaandriver.mvp.models.CancelOrderData;
import com.kolyadruz.dabaandriver.mvp.models.ComingOrderData;
import com.kolyadruz.dabaandriver.mvp.models.CurrentLocationDriverData;
import com.kolyadruz.dabaandriver.mvp.models.DoneOrderData;
import com.kolyadruz.dabaandriver.mvp.models.LoginData;
import com.kolyadruz.dabaandriver.mvp.models.NotificationsData;
import com.kolyadruz.dabaandriver.mvp.models.OrdersListData;
import com.kolyadruz.dabaandriver.mvp.models.TakeOrderData;

import rx.Observable;

public class NetworkService {

    private Api api;

    public NetworkService(Api api) {
        this.api = api;
    }

    public Observable<AutoLoginData> autologin(RequestBody requestBody) {
        return api.autologin(requestBody);
    }

    public Observable<LoginData> login(RequestBody requestBody) {
        return api.login(requestBody);
    }

    public Observable<OrdersListData> getorderslist(RequestBody requestBody) {
        return api.getorderslist(requestBody);
    }

    public Observable<TakeOrderData> takeorder(RequestBody requestBody) {
        return api.takeorder(requestBody);
    }

    public Observable<ComingOrderData> atorderplace(RequestBody requestBody) {
        return api.atorderplace(requestBody);
    }

    public Observable<DoneOrderData> doneorder(RequestBody requestBody) {
        return api.doneorder(requestBody);
    }

    public Observable<CancelOrderData> cancelorder(RequestBody requestBody) {
        return api.cancelorder(requestBody);
    }

    public Observable<ActiveOrderData> getactiveorder(RequestBody requestBody) {
        return api.getactiveorder(requestBody);
    }

    public Observable<DoneOrderData> updatetoken(RequestBody requestBody) {
        return api.updatetoken(requestBody);
    }

    public Observable<NotificationsData> notifications(RequestBody requestBody) {
        return api.notifications(requestBody);
    }

    public Observable<CurrentLocationDriverData> currentlocationdriver(RequestBody requestBody) {
        return api.currentlocationdriver(requestBody);
    }


}
