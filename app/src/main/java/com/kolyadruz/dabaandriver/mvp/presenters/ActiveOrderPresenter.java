package com.kolyadruz.dabaandriver.mvp.presenters;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.dialogs.requestsCodes.ActiveOrderRequestCodes;
import com.kolyadruz.dabaandriver.mvp.NetworkService;
import com.kolyadruz.dabaandriver.mvp.models.ActiveOrderData;
import com.kolyadruz.dabaandriver.mvp.models.CancelOrderData;
import com.kolyadruz.dabaandriver.mvp.models.ComingOrderData;
import com.kolyadruz.dabaandriver.mvp.models.DoneOrderData;
import com.kolyadruz.dabaandriver.mvp.models.OrderModel;
import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.mvp.views.ActiveOrderView;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;
import com.kolyadruz.dabaandriver.utils.RxUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class ActiveOrderPresenter extends BasePresenter<ActiveOrderView> {

    private static final String TAG = "ActiveOrderPresenter";

    @Inject
    NetworkService networkService;

    private Router localRouter;
    private int activeOrderID;

    private String token;
    private String clientPhonenNumber;

    public ActiveOrderPresenter(Router localRouter, int activeOrderID) {
        this.localRouter = localRouter;
        this.activeOrderID = activeOrderID;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        token = PrefUtils.getPrefs().getString("token", "");
        App.getAppComponent().inject(this);
        getActiveOrder();
    }

    public void getActiveOrder() {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = activeOrderID;

        Subscription subscription = networkService.getactiveorder(body)
                .compose(RxUtils.applySchedulers())
                .repeatWhen(completed -> {
                    if (activeOrderID != 0) {
                        return completed.delay(1, TimeUnit.SECONDS);
                    } else {
                        return Observable.empty();
                    }
                })
                .subscribe(new Observer<ActiveOrderData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorDialogWithRequestCode(ActiveOrderRequestCodes.REQUEST_GET_ORDER);
                    }

                    @Override
                    public void onNext(ActiveOrderData activeOrderData) {

                        if (activeOrderData.getErrID() == 0) {

                            OrderModel activeOrder = activeOrderData.getActiveOrder();
                            activeOrderID = activeOrder.getOrderID();
                            clientPhonenNumber = activeOrder.getPhone();

                            if (activeOrderID == 0) {
                                localRouter.replaceScreen(new Screens.EmptyScreen());
                                return;
                            }

                            if (activeOrder.getPhone() != null) {

                                getViewState().showActiveOrderInfo(activeOrder);

                            }

                        } else {
                            getViewState().showErrorMessage(activeOrderData.getErrID());
                        }

                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void atPlace() {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = activeOrderID;

        Subscription subscription = networkService.atorderplace(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<ComingOrderData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorDialogWithRequestCode(ActiveOrderRequestCodes.REQUEST_AT_PLACE);
                    }

                    @Override
                    public void onNext(ComingOrderData comingOrderData) {

                        if (comingOrderData.getErr_id() == 0) {

                            Log.d("OrdersList.AtPlace:", "Статус успешно изменен");

                        } else {

                            getViewState().showErrorMessage(comingOrderData.getErr_id());

                        }

                    }
                });
        unsubscribeOnDestroy(subscription);
    }

    public void completeOrder() {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = activeOrderID;

        Subscription subscription = networkService.doneorder(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<DoneOrderData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorDialogWithRequestCode(ActiveOrderRequestCodes.REQUEST_COMPLETE_ORDER);
                    }

                    @Override
                    public void onNext(DoneOrderData doneOrderData) {
                        if (doneOrderData.getErr_id() == 0) {

                            Log.d("OrdersList.DoneOrder:", "Статус успешно изменен");

                        } else {
                            getViewState().showErrorMessage(doneOrderData.getErr_id());
                        }
                    }
                });

        unsubscribeOnDestroy(subscription);


    }

    public void cancelOrder() {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = activeOrderID;
        body.reason_id = 3;

        Subscription subscription = networkService.cancelorder(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<CancelOrderData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorDialogWithRequestCode(ActiveOrderRequestCodes.REQUEST_CANCEL_ORDER);
                    }

                    @Override
                    public void onNext(CancelOrderData cancelOrderData) {

                        if (cancelOrderData.getErr_id() == 0) {
                            Log.d("OrdersList.CancelOrder:", "Статус успешно изменен");
                        } else {
                            getViewState().showErrorMessage(cancelOrderData.getErr_id());
                        }

                    }
                });
        unsubscribeOnDestroy(subscription);
    }

    public void callToClient() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel: +" + clientPhonenNumber));

        getViewState().startIntent(intent);
    }
}
