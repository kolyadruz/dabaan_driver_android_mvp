package com.kolyadruz.dabaandriver.mvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComingOrderData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    public Integer getErr_id() {
        return err_id;
    }

    public void setErr_id(Integer err_id) {
        this.err_id = err_id;
    }
}
