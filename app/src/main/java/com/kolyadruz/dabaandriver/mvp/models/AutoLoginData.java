package com.kolyadruz.dabaandriver.mvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AutoLoginData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    @SerializedName("notifications")
    @Expose
    private Integer notifications;

    @SerializedName("order")
    @Expose
    private OrderModel order;

    public Integer getErrID() {
        return err_id;
    }

    public void setErrID(Integer err_id) {
        this.err_id = err_id;
    }

    public Integer getNotifications() {
        return notifications;
    }

    public void setNotifications(Integer notifications) {
        this.notifications = notifications;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }
}
