package com.kolyadruz.dabaandriver.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.dabaandriver.mvp.views.UserSettingsView;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class UserSettingsPresenter extends BasePresenter<UserSettingsView> {

    private Router router;

    public UserSettingsPresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void onExitClicked() {
        PrefUtils.getEditor().putString("token", "").commit();
        router.replaceScreen(new Screens.AuthScreen());
    }

}
