package com.kolyadruz.dabaandriver.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.mvp.NetworkService;
import com.kolyadruz.dabaandriver.mvp.models.LoginData;
import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.mvp.views.LoginView;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;
import com.kolyadruz.dabaandriver.utils.RxUtils;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {

    private static final String TAG = "LoginPresenter";

    @Inject
    NetworkService networkService;

    private Router router;

    private String phone;
    private String pass;

    public LoginPresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        App.getAppComponent().inject(this);
    }

    public void setLoginData(String phone, String pass) {
        this.phone = phone;
        this.pass = pass;

        login();
    }

    public void login() {

        String fbtoken = PrefUtils.getPrefs().getString("fbtoken", "");

        RequestBody body = new RequestBody();
        body.fb_token = fbtoken;
        body.phone = phone;
        body.pass = pass;

        Subscription subscription = networkService.login(body)
            .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<LoginData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.toString());
                        getViewState().showConnectionErrorDialogWithRequestCode(1);
                    }

                    @Override
                    public void onNext(LoginData loginData) {

                        if (loginData.getErrID() == 0) {

                            PrefUtils.getEditor().putString("token", loginData.getToken()).commit();
                            PrefUtils.getEditor().putInt("notifications", loginData.getNotifications()).commit();

                            Log.d("Login:","Переход в OrdersList.java");

                            //getViewState().startMain(0);

                            router.replaceScreen(new Screens.MainScreen());

                        } else {
                            getViewState().showErrorMessage(loginData.getErrID());
                        }

                    }
                });
        unsubscribeOnDestroy(subscription);
    }
}
