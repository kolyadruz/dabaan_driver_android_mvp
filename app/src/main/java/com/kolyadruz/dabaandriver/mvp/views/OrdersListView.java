package com.kolyadruz.dabaandriver.mvp.views;

import com.kolyadruz.dabaandriver.mvp.models.OrderModel;

import java.util.List;

public interface OrdersListView extends BaseMvpView {

    void updateUserBalance(String userBalance);
    void updateOrdersList(List<OrderModel> orders);

    void showOrdersEmptyText(boolean show);

    void showArrivalVariantDialog(String address);

    void showActiveOrder(int activeOrderID);
}
