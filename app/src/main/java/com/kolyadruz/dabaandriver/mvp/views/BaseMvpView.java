package com.kolyadruz.dabaandriver.mvp.views;

import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView {

    void showErrorMessage(int errorID);
    void showConnectionErrorDialogWithRequestCode(int requestCode);

}
