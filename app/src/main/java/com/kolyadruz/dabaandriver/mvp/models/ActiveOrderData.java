package com.kolyadruz.dabaandriver.mvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActiveOrderData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    @SerializedName("order")
    @Expose
    private OrderModel order;

    public Integer getErrID() {
        return err_id;
    }

    public void setErrID(Integer err_id) {
        this.err_id = err_id;
    }

    public OrderModel getActiveOrder() {
        return order;
    }

    public void setActiveOrder(OrderModel order) {
        this.order = order;
    }
}
