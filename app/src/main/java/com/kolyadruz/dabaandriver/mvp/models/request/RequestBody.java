package com.kolyadruz.dabaandriver.mvp.models.request;

public class RequestBody {

    public String token;
    public String fb_token;
    public String phone;
    public String pass;
    public Integer order_id;
    public Integer reason_id;
    public Integer arrivalTime;
    public Double lat;
    public Double lon;

}
