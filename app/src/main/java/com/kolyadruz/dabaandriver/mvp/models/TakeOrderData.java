package com.kolyadruz.dabaandriver.mvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TakeOrderData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    public Integer getErrID() {
        return err_id;
    }

    public void setErrID(Integer err_id) {
        this.err_id = err_id;
    }
}
