package com.kolyadruz.dabaandriver.mvp.views;

import android.content.Intent;

import com.kolyadruz.dabaandriver.mvp.models.OrderModel;

public interface ActiveOrderView extends BaseMvpView {

    void showActiveOrderInfo(OrderModel activeOrder);
    void startIntent(Intent intent);

}
