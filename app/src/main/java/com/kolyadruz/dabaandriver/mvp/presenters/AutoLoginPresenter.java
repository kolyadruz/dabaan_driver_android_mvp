package com.kolyadruz.dabaandriver.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.mvp.NetworkService;
import com.kolyadruz.dabaandriver.mvp.models.AutoLoginData;
import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.mvp.views.AutoLoginView;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;
import com.kolyadruz.dabaandriver.utils.RxUtils;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class AutoLoginPresenter extends BasePresenter<AutoLoginView> {

    private static final String TAG = "AutoLoginPresenter";

    @Inject
    NetworkService networkService;

    private Router router;

    public AutoLoginPresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        App.getAppComponent().inject(this);
        super.onFirstViewAttach();
        autoLogin();
    }

    public void autoLogin() {

        String token = PrefUtils.getPrefs().getString("token", "");

        RequestBody body = new RequestBody();
        body.token = token;

        Subscription subscription = networkService.autologin(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<AutoLoginData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ");
                        getViewState().showConnectionErrorDialogWithRequestCode(1);
                    }

                    @Override
                    public void onNext(AutoLoginData autoLoginData) {

                        Log.d(TAG, "onNext: ");

                        if (autoLoginData.getErrID() == 0) {

                            Log.d("AutoLogin:", "Токен рабочий");

                            PrefUtils.getEditor().putInt("notifications", autoLoginData.getNotifications()).commit();

                            router.replaceScreen(new Screens.MainScreen());

                        } else {

                            getViewState().showErrorMessage(autoLoginData.getErrID());

                        }

                    }
                });

        unsubscribeOnDestroy(subscription);



    }
}
