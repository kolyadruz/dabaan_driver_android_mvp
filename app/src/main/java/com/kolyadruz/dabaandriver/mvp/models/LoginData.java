package com.kolyadruz.dabaandriver.mvp.models;

import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("err_id")
    private Integer err_id;
    @SerializedName("token")
    private String token;
    @SerializedName("notifications")
    private Integer notifications;

    public Integer getErrID() {
        return err_id;
    }

    public void setErrID(Integer err_id) {
        this.err_id = err_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getNotifications() {
        return notifications;
    }

    public void setNotifications(Integer notifications) {
        this.notifications = notifications;
    }
}
