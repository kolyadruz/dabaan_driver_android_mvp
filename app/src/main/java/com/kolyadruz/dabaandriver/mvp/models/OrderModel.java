package com.kolyadruz.dabaandriver.mvp.models;

import com.google.gson.annotations.SerializedName;

public class OrderModel {

    @SerializedName("order_id")
    private Integer order_id;

    @SerializedName("phone")
    private String phone;
    @SerializedName("point_a")
    private String point_a;
    @SerializedName("point_b")
    private String point_b;
    @SerializedName("price")
    private String price;
    @SerializedName("prim")
    private String prim;

    @SerializedName("status")
    private Integer status;
    @SerializedName("payment_id")
    private Integer payment_id;
    @SerializedName("fromApp")
    private Integer fromApp;

    public Integer getOrderID() {
        return order_id;
    }

    public void setOrderID(Integer order_id) {
        this.order_id = order_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPointA() {
        return point_a;
    }

    public void setPointA(String point_a) {
        this.point_a = point_a;
    }

    public String getPointB() {
        return point_b;
    }

    public void setPointB(String point_b) {
        this.point_b = point_b;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrim() {
        return prim;
    }

    public void setPrim(String prim) {
        this.prim = prim;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPaymentID() {
        return payment_id;
    }

    public void setPaymentID(Integer payment_id) {
        this.payment_id = payment_id;
    }

    public Integer getFromApp() {
        return fromApp;
    }

    public void setFromApp(Integer fromApp) {
        this.fromApp = fromApp;
    }
}
