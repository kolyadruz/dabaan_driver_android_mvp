package com.kolyadruz.dabaandriver.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.dialogs.requestsCodes.OrdersListRequestsCodes;
import com.kolyadruz.dabaandriver.mvp.NetworkService;
import com.kolyadruz.dabaandriver.mvp.models.OrderModel;
import com.kolyadruz.dabaandriver.mvp.models.OrdersListData;
import com.kolyadruz.dabaandriver.mvp.models.TakeOrderData;
import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.mvp.views.OrdersListView;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;
import com.kolyadruz.dabaandriver.utils.RxUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class OrdersListPresenter extends BasePresenter<OrdersListView> {

    private static final String TAG = "OrdersListPresenter";

    private Router globalRouter;

    private String token;

    private List<OrderModel> newOrders;
    private OrderModel selectedOrder;

    private int arrivalTime;

    @Inject
    NetworkService networkService;

    public OrdersListPresenter(Router globalRouter) {
        this.globalRouter = globalRouter;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        token = PrefUtils.getPrefs().getString("token", "");

        App.getAppComponent().inject(this);

        getNewOrders();
    }

    public void getNewOrders() {

        RequestBody body = new RequestBody();
        body.token = token;

        Subscription subscription = networkService.getorderslist(body)
                .compose(RxUtils.applySchedulers())
                .repeatWhen(completed -> completed.delay(5, TimeUnit.SECONDS))
                .subscribe(new Observer<OrdersListData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ");
                        getViewState().showConnectionErrorDialogWithRequestCode(OrdersListRequestsCodes.REQUEST_GET_NEW_ORDERS);
                    }

                    @Override
                    public void onNext(OrdersListData ordersListData) {

                        if (ordersListData.getErrID() == 0) {

                            if (ordersListData.getBalance() != null) {
                                getViewState().updateUserBalance(ordersListData.getBalance());
                            }

                            getViewState().showOrdersEmptyText(ordersListData.getOrders().size() == 0);

                            if (ordersListData.getActiveOrder().getOrderID() != 0) {
                                getViewState().showActiveOrder(ordersListData.getActiveOrder().getOrderID());
                            }

                            newOrders = ordersListData.getOrders();
                            getViewState().updateOrdersList(newOrders);

                        } else {
                            getViewState().showErrorMessage(ordersListData.getErrID());
                        }

                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void setArrivalTime(Integer arrivalTime) {
        this.arrivalTime = arrivalTime;
        takeOrder();
    }

    public void takeOrder() {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = selectedOrder.getOrderID();
        body.arrivalTime = arrivalTime;

        Subscription subscription = networkService.takeorder(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<TakeOrderData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ");
                        getViewState().showConnectionErrorDialogWithRequestCode(OrdersListRequestsCodes.REQUEST_TAKE_ORDER);
                    }

                    @Override
                    public void onNext(TakeOrderData takeOrderData) {

                        Log.d(TAG, "onNext: ");
                        
                        if (takeOrderData.getErrID() == 0) {

                            getViewState().showActiveOrder(selectedOrder.getOrderID());

                        } else {

                            getViewState().showErrorMessage(takeOrderData.getErrID());

                        }

                    }
                });
        unsubscribeOnDestroy(subscription);
    }

    public void onBonusInfoClicked() {
        globalRouter.navigateTo(new Screens.BonusInfoScreen());
    }

    public void onSettingsClicked() {
        globalRouter.navigateTo(new Screens.SettingsScreen());
    }

    public void onItemClick(int position) {

        selectedOrder = newOrders.get(position);
        getViewState().showArrivalVariantDialog(selectedOrder.getPointA());

    }
}
