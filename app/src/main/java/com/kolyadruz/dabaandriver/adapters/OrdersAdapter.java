package com.kolyadruz.dabaandriver.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.mvp.models.OrderModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kolyadruz on 04.12.2017.
 */

public class OrdersAdapter extends BaseAdapter {


    private List<OrderModel> orders = new ArrayList<>();

    private LayoutInflater inflater=null;

    //public order_id;

    public OrdersAdapter(Activity activity) {

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return orders.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        OrdersListCell ordersListCell;
        if(editingCell == null) {
            cellView = inflater.inflate(R.layout.item_order, null);
            ordersListCell = new OrdersListCell(cellView);
            cellView.setTag(ordersListCell);
        } else {
            ordersListCell = (OrdersListCell) cellView.getTag();
        }

        final OrderModel order = orders.get(position);

        ordersListCell.pointAtxt.setText(order.getPointA());
        ordersListCell.pointBtxt.setText(order.getPointB());
        ordersListCell.costTxt.setText(order.getPrice());
        ordersListCell.noteTxt.setText(order.getPrim());

        if(order.getPaymentID() == 0) {
            ordersListCell.bgcolor.setBackgroundColor(Color.WHITE);
        } else {
            ordersListCell.bgcolor.setBackgroundColor(Color.YELLOW);
        }

        if (order.getFromApp() == 0) {
            ordersListCell.phoneLogo.setVisibility(View.INVISIBLE);
        } else {
            ordersListCell.phoneLogo.setVisibility(View.VISIBLE);
        }

        Log.d("OrdersAdapter", "Added CELL");

        return cellView;
    }

    class OrdersListCell {
        public TextView pointAtxt;
        public TextView pointBtxt;
        public TextView costTxt;
        public TextView noteTxt;
        public ConstraintLayout bgcolor;

        public ImageView phoneLogo;

        public OrdersListCell(View itemView) {
            pointAtxt = itemView.findViewById(R.id.pointAtxt);
            pointBtxt = itemView.findViewById(R.id.pointBtxt);
            costTxt = itemView.findViewById(R.id.costTxt);
            noteTxt = itemView.findViewById(R.id.primTxt);
            bgcolor = itemView.findViewById(R.id.bgcolor);
            phoneLogo = itemView.findViewById(R.id.phoneLogo);
        }
    }

    public void updateData(List<OrderModel> orders) {

        this.orders = orders;
        notifyDataSetChanged();

    }

}