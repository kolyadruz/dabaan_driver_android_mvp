package com.kolyadruz.dabaandriver.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kolyadruz.dabaandriver.app.App;

public class DialogWithVariants extends DialogFragment {

    public static final String TAG_RESULT_SELECTED = "result";

    private static final String TAG_TITLE = "title";
    private static final String TAG_ACTIONS_ARRAY = "actions";
    private static final String TAG_VALES_ARRAY = "values";

    private String title;
    private String[] actions;
    private int[] values;

    private Context context;

    public static DialogWithVariants getNewInstance(String title, String[] actions, int[] values) {

        Bundle args = new Bundle();

        args.putString(TAG_TITLE, title);
        args.putIntArray(TAG_VALES_ARRAY, values);
        args.putStringArray(TAG_ACTIONS_ARRAY, actions);

        DialogWithVariants fragment = new DialogWithVariants();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            this.title = getArguments().getString(TAG_TITLE);
            this.actions = getArguments().getStringArray(TAG_ACTIONS_ARRAY);
            this.values = getArguments().getIntArray(TAG_VALES_ARRAY);

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setItems(actions, (dialog, item) -> {
            Intent intent = new Intent();

            if (item < actions.length) {
                intent.putExtra(TAG_RESULT_SELECTED, values[item]);
            } else {
                return;
            }

            try {

                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

            } catch (NullPointerException ex) {

                Toast.makeText(App.getAppComponent().getContext(), "Ошибка диалога", Toast.LENGTH_LONG).show();

            }
        });
        builder.setNegativeButton("Закрыть", (dialog, which) -> {
            this.dismiss();
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
