package com.kolyadruz.dabaandriver.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kolyadruz.dabaandriver.utils.ErrorMessageUtils;

public class DialogErrorIDMessage extends DialogFragment {

    private static final String TAG_ERR_ID = "message";

    private int errID;
    private Context context;

    public static DialogErrorIDMessage getNewInstance(int errID) {

        Bundle args = new Bundle();

        args.putInt(TAG_ERR_ID, errID);

        DialogErrorIDMessage fragment = new DialogErrorIDMessage();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.errID = getArguments().getInt(TAG_ERR_ID);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(ErrorMessageUtils.getErrorMessage(errID));
        builder.setPositiveButton("ОК", (dialog, which) -> {
            this.dismiss();
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
