package com.kolyadruz.dabaandriver.dialogs.requestsCodes;

public final class OrdersListRequestsCodes {

    public static final int REQUEST_GET_NEW_ORDERS = 1;
    public static final int REQUEST_TAKE_ORDER = 2;
    public static final int REQUEST_ARRIVAL_TIME = 3;

}