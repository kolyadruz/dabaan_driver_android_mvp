package com.kolyadruz.dabaandriver.dialogs.requestsCodes;

public final class ActiveOrderRequestCodes {

    public static final int REQUEST_GET_ORDER = 1;
    public static final int REQUEST_AT_PLACE = 2;
    public static final int REQUEST_COMPLETE_ORDER = 3;
    public static final int REQUEST_CANCEL_ORDER = 4;
    public static final int REQUEST_CANCEL_ACTION = 5;
    public static final int REQUEST_COMPLETE_ACTION = 6;

}
