package com.kolyadruz.dabaandriver.screens;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.kolyadruz.dabaandriver.ui.auth.activities.Auth;
import com.kolyadruz.dabaandriver.ui.auth.fragments.AutoLogin;
import com.kolyadruz.dabaandriver.ui.auth.fragments.Login;
import com.kolyadruz.dabaandriver.ui.main.fragments.ActiveOrder;
import com.kolyadruz.dabaandriver.ui.main.fragments.BalanceInfo;
import com.kolyadruz.dabaandriver.ui.main.activities.MainActivity;
import com.kolyadruz.dabaandriver.ui.main.fragments.EmptyActiveOrder;
import com.kolyadruz.dabaandriver.ui.main.fragments.OrdersList;
import com.kolyadruz.dabaandriver.ui.main.fragments.UserSettings;

import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class AuthScreen extends SupportAppScreen {

        @Override
        public Intent getActivityIntent(Context context) {
            return new Intent(context, Auth.class);
        }
    }

    public static final class AutoLoginScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new AutoLogin();
        }

    }

    public static final class LoginScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new Login();
        }
    }

    public static final class MainScreen extends SupportAppScreen {

        @Override
        public Intent getActivityIntent(Context context) {
            return new Intent(context, MainActivity.class);
        }
    }

    public static final class OrdersListScreen extends SupportAppScreen {

        int activeOrderID;

        public OrdersListScreen(int activeOrderID) {

            this.activeOrderID = activeOrderID;
        }

        @Override
        public Fragment getFragment() {
            return new OrdersList();
        }
    }


    public static final class SettingsScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new UserSettings();
        }

    }

    public static final class BonusInfoScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new BalanceInfo();
        }

    }

    public static final class ActiveOrderScreen extends SupportAppScreen {

        private int activeOrderID;

        public ActiveOrderScreen(int activeOrderID) {
            this.activeOrderID = activeOrderID;
        }

        @Override
        public Fragment getFragment() {
            return ActiveOrder.getNewInstance(activeOrderID);
        }

    }

    public static final class EmptyScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new EmptyActiveOrder();
        }

    }

}
