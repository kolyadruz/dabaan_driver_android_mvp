package com.kolyadruz.dabaandriver;

/**
 * Created by kolyadruz on 28.03.2018.
 */

import android.content.ContentResolver;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.mvp.NetworkService;
import com.kolyadruz.dabaandriver.mvp.models.DoneOrderData;
import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.ui.auth.fragments.AutoLogin;
import com.kolyadruz.dabaandriver.utils.NotificationUtils;
import com.kolyadruz.dabaandriver.utils.PrefUtils;
import com.kolyadruz.dabaandriver.utils.RxUtils;
import com.kolyadruz.dabaandriver.vo.NotificationVO;

import java.util.Map;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Inject
    NetworkService networkService;

    private static final String TAG = "MyFirebaseMsgingService";
    private static final String TITLE = "title";
    private static final String EMPTY = "";
    private static final String MESSAGE = "message";
    private static final String IMAGE = "image";
    private static final String ACTION = "action";
    private static final String DATA = "data";
    private static final String ACTION_DESTINATION = "action_destination";

    private Subscription subscription;

    @Override
    public void onCreate() {
        super.onCreate();
        App.getAppComponent().inject(this);
    }

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            handleData(data);

        } else if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification());
        }// Check if message contains a notification payload.

    }
    // [END receive_message]

    private void handleNotification(RemoteMessage.Notification RemoteMsgNotification) {
        String message = RemoteMsgNotification.getBody();
        String title = RemoteMsgNotification.getTitle();
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);

        Intent resultIntent = new Intent(getApplicationContext(), AutoLogin.class);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());

        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();

    }

    private void handleData(Map<String, String> data) {
        String title = data.get(TITLE);
        String message = data.get(MESSAGE);
        String iconUrl = data.get(IMAGE);
        String action = data.get(ACTION);
        String actionDestination = data.get(ACTION_DESTINATION);
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);

        String iconDabaan = ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + getApplicationContext().getPackageName() + "/drawable/ic_notification";

        notificationVO.setIconUrl(iconDabaan);
        notificationVO.setAction(action);
        notificationVO.setActionDestination(actionDestination);

        Intent resultIntent = new Intent(getApplicationContext(), AutoLogin.class);

        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());


        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();

    }

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        FirebaseMessaging.getInstance().subscribeToTopic("drivers");

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String fbtoken) {
        // TODO: Implement this method to send token to your app server.

        RequestBody body = new RequestBody();
        body.token = PrefUtils.getPrefs().getString("token", "");
        body.fb_token = fbtoken;

        subscription = networkService.updatetoken(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<DoneOrderData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(DoneOrderData doneOrderData) {

                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        subscription.unsubscribe();
    }
}

