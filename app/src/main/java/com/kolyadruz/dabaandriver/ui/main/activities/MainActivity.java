package com.kolyadruz.dabaandriver.ui.main.activities;

import android.os.Bundle;
import android.view.WindowManager;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.common.OnThemeChangedListener;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

public class MainActivity extends MvpAppCompatActivity implements OnThemeChangedListener {

    @Inject
    NavigatorHolder navigatorHolder;

    private Navigator navigator = new SupportAppNavigator(this, R.id.page_container);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App.getNavigationComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTheme(PrefUtils.getPrefs().getInt("theme", R.id.standard));

        if (savedInstanceState == null) {
            navigator.applyCommands(new Command[]{new Replace(
                    new Screens.OrdersListScreen(0)
                )
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onThemeChanged(int styleID) {

        PrefUtils.getEditor().putInt("theme", styleID).commit();
        recreate();

    }
}
