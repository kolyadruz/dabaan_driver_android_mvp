package com.kolyadruz.dabaandriver.ui.auth.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.dabaandriver.dialogs.DialogConnectionError;
import com.kolyadruz.dabaandriver.dialogs.DialogErrorIDMessage;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.mvp.presenters.AutoLoginPresenter;
import com.kolyadruz.dabaandriver.mvp.views.AutoLoginView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
public class AutoLogin extends MvpAppCompatFragment implements AutoLoginView {

    private static final String TAG = "AutoLogin";

    private static final int REQUEST_LOGIN = 1;

    @Inject
    Router router;

    @InjectPresenter
    AutoLoginPresenter presenter;

    @ProvidePresenter
    AutoLoginPresenter autoLoginPresenter() {
        return new AutoLoginPresenter(router);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_autologin, container, false);
        return rootView;
    }

    @Override
    public void showErrorMessage(int errorID) {
        DialogFragment dialog = DialogErrorIDMessage.getNewInstance(errorID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorDialogWithRequestCode(int requestCode) {
        DialogFragment fragment = new DialogConnectionError();
        fragment.setTargetFragment(this, requestCode);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1:
                    presenter.autoLogin();
                    break;
                default:
                    break;
            }
        }
    }
}
