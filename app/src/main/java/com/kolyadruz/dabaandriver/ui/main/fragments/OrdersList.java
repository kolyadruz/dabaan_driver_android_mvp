package com.kolyadruz.dabaandriver.ui.main.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.dialogs.DialogConnectionError;
import com.kolyadruz.dabaandriver.dialogs.DialogErrorIDMessage;
import com.kolyadruz.dabaandriver.dialogs.DialogWithVariants;
import com.kolyadruz.dabaandriver.dialogs.requestsCodes.OrdersListRequestsCodes;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.adapters.OrdersAdapter;
import com.kolyadruz.dabaandriver.di.modules.subanvigation.LocalCiceroneHolder;
import com.kolyadruz.dabaandriver.di.modules.subanvigation.RouterProvider;
import com.kolyadruz.dabaandriver.mvp.models.OrderModel;
import com.kolyadruz.dabaandriver.mvp.presenters.OrdersListPresenter;
import com.kolyadruz.dabaandriver.mvp.views.OrdersListView;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;

public class OrdersList extends MvpAppCompatFragment implements AdapterView.OnItemClickListener, OrdersListView, RouterProvider {

    private static final String TAG = "OrdersList";

    private Navigator navigator;

    @Inject
    Router globalRouter;

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    @InjectPresenter
    OrdersListPresenter presenter;

    @ProvidePresenter
    OrdersListPresenter ordersListPresenter() {
        return new OrdersListPresenter(globalRouter);
    }

    private static Context context;
    private OrdersAdapter ordersAdapter;

    @BindView(R.id.newOrdersList)
    ListView newOrdersList;

    @BindView(R.id.flag)
    ConstraintLayout flag;

    @BindView(R.id.noOrdersTxt)
    TextView emptyNewOrdersListText;
    @BindView(R.id.balanceTxt)
    TextView userBalanceTxt;

    @BindView(R.id.whiteTint)
    ConstraintLayout whiteTint;

    @BindView(R.id.active_order_frame)
    FrameLayout takenOrderDialog;

    private Unbinder unbinder;

    private Cicerone<Router> getCicerone() {
        return ciceroneHolder.getCicerone(TAG);
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.active_order_frame);
        }
        return navigator;
    }

    @Override
    public Router getLocalRouter() {
        return getCicerone().getRouter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView  = inflater.inflate(R.layout.fragment_orderslist, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        Integer theme = PrefUtils.getPrefs().getInt("theme", 0);
        flag.setVisibility(theme == R.style.PatriotTheme? View.VISIBLE : View.GONE);

        ordersAdapter = new OrdersAdapter((Activity)context);
        newOrdersList.setOnItemClickListener(this);
        newOrdersList.setAdapter(ordersAdapter);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        getCicerone().getNavigatorHolder().setNavigator(getNavigator());
    }

    @Override
    public void onPause() {
        getCicerone().getNavigatorHolder().removeNavigator();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (unbinder != null) unbinder.unbind();
        super.onDestroy();
    }

    @OnClick(R.id.bonusInfoBtn)
    void showBonusInfo() {
        presenter.onBonusInfoClicked();
    }

    @OnClick(R.id.settingsBtn)
    void settingsTapped() {
        presenter.onSettingsClicked();
    }

    @Override
    public void updateUserBalance(String userBalance) {
        userBalanceTxt.setText(userBalance);
    }

    @Override
    public void showOrdersEmptyText(boolean show) {
        emptyNewOrdersListText.setVisibility(show? View.VISIBLE : View.GONE);
        newOrdersList.setVisibility(show? View.GONE : View.VISIBLE);
    }

    @Override
    public void showArrivalVariantDialog(String address) {

        int[] arrivalTimeVariants = {5, 10, 20, 30, 40};
        String[] actions = new String[arrivalTimeVariants.length];

        for (int i = 0; i < arrivalTimeVariants.length; i++) {
            actions[i] = arrivalTimeVariants[i] + " минут";
        }

        DialogFragment fragment = DialogWithVariants.getNewInstance("Прибуду на " + address + " через: ", actions, arrivalTimeVariants);
        fragment.setTargetFragment(this, OrdersListRequestsCodes.REQUEST_ARRIVAL_TIME);
        fragment.show(getFragmentManager(), fragment.getClass().getName());

    }

    @Override
    public void showActiveOrder(int activeOrderID) {
        if (!(getChildFragmentManager().findFragmentById(R.id.active_order_frame) instanceof ActiveOrder)) {
            getLocalRouter().replaceScreen(new Screens.ActiveOrderScreen(activeOrderID));
        }
    }

    @Override
    public void updateOrdersList(List<OrderModel> orders) {
        ordersAdapter.updateData(orders);
    }

    @Override
    public void showErrorMessage(int errorID) {
        DialogFragment fragment = DialogErrorIDMessage.getNewInstance(errorID);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @Override
    public void showConnectionErrorDialogWithRequestCode(int requestCode) {

        DialogFragment fragment = new DialogConnectionError();
        fragment.setTargetFragment(this, requestCode);
        fragment.show(getFragmentManager(), fragment.getClass().getName());

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        presenter.onItemClick(position);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case OrdersListRequestsCodes.REQUEST_ARRIVAL_TIME:
                    presenter.setArrivalTime(data.getIntExtra(DialogWithVariants.TAG_RESULT_SELECTED, 5));
                    break;
                case OrdersListRequestsCodes.REQUEST_GET_NEW_ORDERS:
                    presenter.getNewOrders();
                    break;
                case OrdersListRequestsCodes.REQUEST_TAKE_ORDER:
                    presenter.takeOrder();
                default:
                    break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}