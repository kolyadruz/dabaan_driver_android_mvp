package com.kolyadruz.dabaandriver.ui.main.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.di.modules.subanvigation.RouterProvider;
import com.kolyadruz.dabaandriver.dialogs.DialogErrorIDMessage;
import com.kolyadruz.dabaandriver.dialogs.DialogWithVariants;
import com.kolyadruz.dabaandriver.dialogs.requestsCodes.ActiveOrderRequestCodes;
import com.kolyadruz.dabaandriver.dialogs.DialogConnectionError;
import com.kolyadruz.dabaandriver.mvp.models.OrderModel;
import com.kolyadruz.dabaandriver.mvp.presenters.ActiveOrderPresenter;
import com.kolyadruz.dabaandriver.mvp.views.ActiveOrderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ActiveOrder extends MvpAppCompatFragment implements ActiveOrderView {

    private static final String ARG_ACTIVE_ORDER_ID = "activeOrderID";

    @InjectPresenter
    ActiveOrderPresenter presenter;

    @ProvidePresenter
    ActiveOrderPresenter activeOrderPresenter() {
        return new ActiveOrderPresenter(((RouterProvider)getParentFragment()).getLocalRouter(), getArguments().getInt(ARG_ACTIVE_ORDER_ID));
    }

    private Context context;

    @BindView(R.id.pointAtxt)
    TextView pointAtxt;
    @BindView(R.id.pointBtxt)
    TextView pointBtxt;
    @BindView(R.id.noteTxt)
    TextView noteTxt;
    @BindView(R.id.costTxt)
    TextView costTxt;
    @BindView(R.id.orderStatusTxt)
    TextView orderStatusTxt;
    @BindView(R.id.cancelBtnTextView)
    TextView cancelBtnTextView;

    private Unbinder unbinder;

    public static ActiveOrder getNewInstance(int activeOrderID) {
        ActiveOrder fragment = new ActiveOrder();
        Bundle args = new Bundle();
        args.putInt(ARG_ACTIVE_ORDER_ID, activeOrderID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_active_order, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }

    @OnClick(R.id.atPlaceBtn)
    void atPlace() {
        presenter.atPlace();
    }

    @OnClick(R.id.completeOrderBtn)
    void completedOrder() {
        String[] actions ={"Да, выполнен"};
        int[] values = {1};

        DialogFragment fragment = DialogWithVariants.getNewInstance("Заказ точно выполнен?", actions, values);
        fragment.setTargetFragment(this, ActiveOrderRequestCodes.REQUEST_COMPLETE_ACTION);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @OnClick(R.id.cancelOrderBtn)
    void cancelOrder() {
        String[] actions ={"Да, отменить"};
        int[] values = {1};

        DialogFragment fragment = DialogWithVariants.getNewInstance("Вы точно хотите отменить заказ?", actions, values);
        fragment.setTargetFragment(this, ActiveOrderRequestCodes.REQUEST_CANCEL_ACTION);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @OnClick(R.id.callBtn)
    void callClient() {
        presenter.callToClient();
    }

    @Override
    public void showErrorMessage(int errorID) {
        DialogFragment fragment = DialogErrorIDMessage.getNewInstance(errorID);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @Override
    public void showActiveOrderInfo(OrderModel activeOrder) {

        pointAtxt.setText(activeOrder.getPointA());
        pointBtxt.setText(activeOrder.getPointB());
        costTxt.setText(activeOrder.getPrice());

        noteTxt.setVisibility(activeOrder.getPrim().equals("")? View.GONE : View.VISIBLE);
        noteTxt.setText(activeOrder.getPrim());

        switch (activeOrder.getStatus()) {
            case 4:
                orderStatusTxt.setText("ЗАКАЗ ОТМЕНЕН КЛИЕНТОМ");
                cancelBtnTextView.setText("Закрыть");
                break;
            case 8:
                orderStatusTxt.setText("ЗАКАЗ ОТМЕНЕН ДИСПЕТЧЕРОМ");
                cancelBtnTextView.setText("Закрыть");
                break;
            case 9:
                orderStatusTxt.setText("ВЫ НА МЕСТЕ");
                break;
            default:
                break;
        }

    }

    @Override
    public void startIntent(Intent intent) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.CALL_PHONE},1);
        } else {
            startActivity(intent);
        }
    }

    @Override
    public void showConnectionErrorDialogWithRequestCode(int requestCode) {

        DialogFragment fragment = new DialogConnectionError();
        fragment.setTargetFragment(this, requestCode);
        fragment.show(getFragmentManager(), fragment.getClass().getName());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ActiveOrderRequestCodes.REQUEST_GET_ORDER:
                    presenter.getActiveOrder();
                    break;
                case ActiveOrderRequestCodes.REQUEST_AT_PLACE:
                    presenter.atPlace();
                    break;
                case ActiveOrderRequestCodes.REQUEST_COMPLETE_ORDER:
                    presenter.completeOrder();
                    break;
                case ActiveOrderRequestCodes.REQUEST_CANCEL_ORDER:
                    presenter.cancelOrder();
                    break;
                case ActiveOrderRequestCodes.REQUEST_CANCEL_ACTION:
                    if (data.getIntExtra(DialogWithVariants.TAG_RESULT_SELECTED, -1) == 1) {
                        presenter.cancelOrder();
                    }
                case ActiveOrderRequestCodes.REQUEST_COMPLETE_ACTION:
                    if (data.getIntExtra(DialogWithVariants.TAG_RESULT_SELECTED, -1) == 1) {
                        presenter.completeOrder();
                    }
                default:
                    break;
            }
        }

    }
}
