package com.kolyadruz.dabaandriver.ui.auth.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.WindowManager;

import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.screens.Screens;
import com.kolyadruz.dabaandriver.utils.PrefUtils;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Screen;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

public class Auth extends AppCompatActivity {

    @Inject
    NavigatorHolder navigatorHolder;

    private Navigator navigator = new SupportAppNavigator(this, R.id.page_container);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        if (savedInstanceState == null) {
            navigator.applyCommands(new Command[]{
                new Replace(
                        newScreenByToken()
                )
            });
        }
    }

    private Screen newScreenByToken() {

        String token = PrefUtils.getPrefs().getString("token", "");

        if (token.equals("")) {
            return new Screens.LoginScreen();
        } else {
            return new Screens.AutoLoginScreen();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
