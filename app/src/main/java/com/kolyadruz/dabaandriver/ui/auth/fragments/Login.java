package com.kolyadruz.dabaandriver.ui.auth.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.dialogs.DialogErrorIDMessage;
import com.kolyadruz.dabaandriver.mvp.presenters.LoginPresenter;
import com.kolyadruz.dabaandriver.mvp.views.LoginView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Router;

public class Login extends MvpAppCompatFragment implements LoginView {

    @Inject
    Router router;

    @BindView(R.id.phoneField)
    EditText phoneField;
    @BindView(R.id.passField)
    EditText passField;
    @BindView(R.id.enterButton)
    Button enterButton;

    @InjectPresenter
    LoginPresenter presenter;

    @ProvidePresenter
    LoginPresenter loginPresenter() {
        return new LoginPresenter(router);
    }

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }

    @OnClick(R.id.enterButton)
    void login() {
        presenter.setLoginData(phoneField.getText().toString(), passField.getText().toString());
    }

    @Override
    public void showErrorMessage(int errorID) {
        DialogFragment fragment = DialogErrorIDMessage.getNewInstance(errorID);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @Override
    public void showConnectionErrorDialogWithRequestCode(int requestCode) {
        DialogFragment fragment = DialogErrorIDMessage.getNewInstance(1);
        fragment.setTargetFragment(this, requestCode);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1:
                    presenter.login();
                    break;
                default:
                    break;
            }
        }
    }
}
