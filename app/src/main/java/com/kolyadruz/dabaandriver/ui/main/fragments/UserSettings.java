package com.kolyadruz.dabaandriver.ui.main.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.dabaandriver.app.App;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.common.OnThemeChangedListener;
import com.kolyadruz.dabaandriver.dialogs.DialogWithVariants;
import com.kolyadruz.dabaandriver.mvp.presenters.UserSettingsPresenter;
import com.kolyadruz.dabaandriver.mvp.views.UserSettingsView;
import com.kolyadruz.dabaandriver.utils.PrefUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Router;

public class UserSettings extends MvpAppCompatFragment implements UserSettingsView {

    private static final int REQUEST_THEME = 1;

    @Inject
    Router router;

    @InjectPresenter
    UserSettingsPresenter presenter;

    @ProvidePresenter
    UserSettingsPresenter userSettingsPresenter() {
        return new UserSettingsPresenter(router);
    }

    private Context context;
    private OnThemeChangedListener themeChangedListener;

    @BindView(R.id.flag)
    ConstraintLayout flag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_user_settings, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        Integer theme = PrefUtils.getPrefs().getInt("theme", 0);
        flag.setVisibility(theme == R.style.PatriotTheme? View.VISIBLE : View.GONE);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(unbinder != null) unbinder.unbind();
    }

    @OnClick(R.id.saveButton)
    void saveTapped(){

    }

    @OnClick(R.id.changeThemeBtn)
    void changeThemeTapped(){
        String[] actions ={"Стандартная", "Патриотичная", "Светлая"};
        int[] values = {R.style.StandartTheme, R.style.PatriotTheme, R.style.SilverTheme};

        DialogFragment fragment = DialogWithVariants.getNewInstance("Выберите тему", actions, values);
        fragment.setTargetFragment(this, REQUEST_THEME);
        fragment.show(getFragmentManager(), fragment.getClass().getName());
    }

    @OnClick(R.id.notificationBtn)
    void notificationsTapped() {

        Intent intent = new Intent();
        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
        } else {
            intent.putExtra("app_package", context.getPackageName());
            intent.putExtra("app_uid", context.getApplicationInfo().uid);
        }

        startActivity(intent);

    }

    @OnClick(R.id.exitBtn)
    void exitClicked(){
        presenter.onExitClicked();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_THEME:
                    themeChangedListener.onThemeChanged(data.getIntExtra(DialogWithVariants.TAG_RESULT_SELECTED, -1));
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.themeChangedListener = (OnThemeChangedListener) context;
    }
}
