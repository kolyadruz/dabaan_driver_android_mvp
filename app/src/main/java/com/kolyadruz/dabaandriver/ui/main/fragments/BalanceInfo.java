package com.kolyadruz.dabaandriver.ui.main.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.utils.PrefUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BalanceInfo extends MvpAppCompatFragment {

    @BindView(R.id.flag)
    ConstraintLayout flag;

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_balance_info, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        Integer theme = PrefUtils.getPrefs().getInt("theme", 0);
        flag.setVisibility(theme == R.style.PatriotTheme? View.VISIBLE : View.GONE);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(unbinder != null) unbinder.unbind();
    }
}
