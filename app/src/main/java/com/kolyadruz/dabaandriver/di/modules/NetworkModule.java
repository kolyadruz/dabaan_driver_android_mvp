package com.kolyadruz.dabaandriver.di.modules;

import com.kolyadruz.dabaandriver.app.Api;
import com.kolyadruz.dabaandriver.mvp.NetworkService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class NetworkModule {

    @Provides
    @Singleton
    public NetworkService provideNetworkService(Api api) {
        return new NetworkService(api);
    }

}
