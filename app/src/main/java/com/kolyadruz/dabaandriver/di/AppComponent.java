package com.kolyadruz.dabaandriver.di;

import android.content.Context;

import com.kolyadruz.dabaandriver.MyFirebaseMessagingService;
import com.kolyadruz.dabaandriver.mvp.presenters.ActiveOrderPresenter;
import com.kolyadruz.dabaandriver.mvp.presenters.AutoLoginPresenter;
import com.kolyadruz.dabaandriver.mvp.presenters.LoginPresenter;
import com.kolyadruz.dabaandriver.mvp.presenters.OrdersListPresenter;
import com.kolyadruz.dabaandriver.di.modules.ContextModule;
import com.kolyadruz.dabaandriver.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class, NetworkModule.class})

public interface AppComponent {

    Context getContext();

    void inject(AutoLoginPresenter autoLoginPresenter);
    void inject(LoginPresenter loginPresenter);
    void inject(OrdersListPresenter ordersListPresenter);
    void inject(ActiveOrderPresenter activeOrderPresenter);

    void inject(MyFirebaseMessagingService messagingService);
}