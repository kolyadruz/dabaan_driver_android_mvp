package com.kolyadruz.dabaandriver.di.modules.subanvigation;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {
    Router getLocalRouter();
}
