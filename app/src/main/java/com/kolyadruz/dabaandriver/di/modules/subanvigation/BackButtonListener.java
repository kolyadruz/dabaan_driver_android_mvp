package com.kolyadruz.dabaandriver.di.modules.subanvigation;

public interface BackButtonListener {

    boolean onBackPressed();

}
