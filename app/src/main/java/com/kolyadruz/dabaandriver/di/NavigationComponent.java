package com.kolyadruz.dabaandriver.di;

import com.kolyadruz.dabaandriver.di.modules.LocalNavigationModule;
import com.kolyadruz.dabaandriver.di.modules.NavigationModule;
import com.kolyadruz.dabaandriver.ui.auth.activities.Auth;
import com.kolyadruz.dabaandriver.ui.auth.fragments.AutoLogin;
import com.kolyadruz.dabaandriver.ui.auth.fragments.Login;
import com.kolyadruz.dabaandriver.ui.main.fragments.ActiveOrder;
import com.kolyadruz.dabaandriver.ui.main.activities.MainActivity;
import com.kolyadruz.dabaandriver.ui.main.fragments.OrdersList;
import com.kolyadruz.dabaandriver.ui.main.fragments.UserSettings;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NavigationModule.class, LocalNavigationModule.class})
public interface NavigationComponent {

    void inject(Auth auth);
    void inject(AutoLogin autoLogin);
    void inject(Login login);

    void inject(MainActivity mainActivity);
    void inject(OrdersList ordersList);
    void inject(UserSettings userSettings);
    void inject(ActiveOrder activeOrder);

}
