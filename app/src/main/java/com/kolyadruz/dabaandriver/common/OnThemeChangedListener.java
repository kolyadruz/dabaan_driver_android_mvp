package com.kolyadruz.dabaandriver.common;

public interface OnThemeChangedListener {

    void onThemeChanged(int styleID);

}
