package com.kolyadruz.dabaandriver.app;

import com.kolyadruz.dabaandriver.mvp.models.request.RequestBody;
import com.kolyadruz.dabaandriver.mvp.models.ActiveOrderData;
import com.kolyadruz.dabaandriver.mvp.models.AutoLoginData;
import com.kolyadruz.dabaandriver.mvp.models.CancelOrderData;
import com.kolyadruz.dabaandriver.mvp.models.ComingOrderData;
import com.kolyadruz.dabaandriver.mvp.models.CurrentLocationDriverData;
import com.kolyadruz.dabaandriver.mvp.models.DoneOrderData;
import com.kolyadruz.dabaandriver.mvp.models.LoginData;
import com.kolyadruz.dabaandriver.mvp.models.NotificationsData;
import com.kolyadruz.dabaandriver.mvp.models.OrdersListData;
import com.kolyadruz.dabaandriver.mvp.models.TakeOrderData;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface Api {

    @POST("drv_autologin")
    Observable<AutoLoginData> autologin(@Body RequestBody requestBody);

    @POST("drvlogin")
    Observable<LoginData> login(@Body RequestBody requestBody);

    @POST("orderslist")
    Observable<OrdersListData> getorderslist(@Body RequestBody requestBody);

    @POST("takeorder")
    Observable<TakeOrderData> takeorder(@Body RequestBody requestBody);

    @POST("drv_coming_order")
    Observable<ComingOrderData> atorderplace(@Body RequestBody requestBody);

    @POST("drv_done_order")
    Observable<DoneOrderData> doneorder(@Body RequestBody requestBody);

    @POST("drv_cancelorder")
    Observable<CancelOrderData> cancelorder(@Body RequestBody requestBody);

    @POST("drv_activeorder")
    Observable<ActiveOrderData> getactiveorder(@Body RequestBody requestBody);

    @POST("updatetoken")
    Observable<DoneOrderData> updatetoken(@Body RequestBody requestBody);

    @POST("drv_notifications")
    Observable<NotificationsData> notifications(@Body RequestBody requestBody);

    @POST("currentlocationdriver")
    Observable<CurrentLocationDriverData> currentlocationdriver(@Body RequestBody requestBody);
}
