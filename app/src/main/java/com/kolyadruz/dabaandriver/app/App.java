package com.kolyadruz.dabaandriver.app;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.kolyadruz.dabaandriver.BuildConfig;
import com.kolyadruz.dabaandriver.di.DaggerNavigationComponent;
import com.kolyadruz.dabaandriver.di.NavigationComponent;
import com.kolyadruz.dabaandriver.utils.Global;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.di.AppComponent;
import com.kolyadruz.dabaandriver.di.DaggerAppComponent;
import com.kolyadruz.dabaandriver.di.modules.ContextModule;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class App extends Application {

    private static AppComponent sAppComponent;
    private static NavigationComponent sNavigationComponent;
    private static Socket mSocket;

    {
        try {
            mSocket = IO.socket(Global.SOCKET_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel(this);

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        sNavigationComponent = DaggerNavigationComponent.create();
    }

    @SuppressLint("NewApi")
    public  void createNotificationChannel(@NonNull Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int NOTIFICATION_COLOR = context.getResources().getColor(R.color.colorPrimary);
            Uri NOTIFICATION_SOUND_URI = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.homus);
            long[] VIBRATE_PATTERN    = {0, 500};



            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = getString(R.string.default_notification_channel_id);
            if(manager.getNotificationChannel(channelId)==null) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Новые заказы",
                        NotificationManager.IMPORTANCE_DEFAULT);
                manager.createNotificationChannel(channel);
            }

        }
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }

    public static NavigationComponent getNavigationComponent() {
        return sNavigationComponent;
    }

}
