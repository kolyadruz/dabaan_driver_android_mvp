package com.kolyadruz.dabaandriver.utils;

public class ErrorMessageUtils {

    public static String getErrorMessage(int errID) {

        switch (errID) {
            case 1:
                return "Указан неправильный номер телефона";
            case 2:
                return  "Повторите попытку позднее";
            case 3:
                return "Ошибка работы токена";
            case 4:
                return "Заполните все поля";
            case 5:
                return  "Ваш аккаунт неактивен";
            case 6:
                return "Пользователь отсутствует или заблокирован";
            case 7:
                return "Заказ закрыт, или отменен";
            case 8:
                return "Недостаточно средств на балансе";
            case 9:
                return "Заказ уже принят";
            case 10:
                return "Ошибка интернет соединения";
            default:
                return null;
        }

    }

}
